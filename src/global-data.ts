import {Connection} from 'typeorm';

interface IGlobalData {
    connection: Connection;
}

export const GlobalData: IGlobalData = {
    // @ts-ignore
    connection: undefined,
};