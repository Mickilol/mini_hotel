import 'reflect-metadata';
import {createConnection} from 'typeorm';
import * as express from 'express';
import * as BodyParser from 'body-parser';
import {GlobalData} from "./global-data";
import {AccountAuthorizationHandler} from "./account/handlers/account.authorization.handler";
import {AccountSignInHandler} from "./account/handlers/account.sign-in.handler";
import {AdminAddApartmentHandler} from "./account/handlers/admin.add-apartment.handler";
import {AdminAddStaffHandler} from "./account/handlers/admin.add-staff.handler";
import {AdminAddRentHandler} from "./account/handlers/admin.add-rent.handler";
import {AdminAddApartmentFacilitiesHandler} from "./account/handlers/admin.add-apartment-facilities.handler";
import {AdminAddApartmentExpenseHandler} from "./account/handlers/admin.add-apartment-expense.handler";
import {AdminAddPayoutHandler} from "./account/handlers/admin.add-payout.handler";
import {AccountAddReviewHandler} from "./account/handlers/account.add-review.handler";
import {AdminAddStaffApartmentHandler} from "./account/handlers/admin.add-staff-apartments.handler";
import {AdminUpdateApartmentHandler} from "./account/handlers/admin.update-apartment.handler";
import {AdminDeleteApartmentHandler} from "./account/handlers/admin.delete-apartmet.handler";
import {DashboardGetApartmentsHandler} from "./account/handlers/dashboard.get-apartments.handler";
import {AdminGetStaffListHandler} from "./account/handlers/admin.get-staff-list.handler";
import {DashboardGetApartmentFacilitiesHandler} from "./account/handlers/dashboard.get-apartment-facilities.handler";
import {AdminGetApartmentExpenseHandler} from "./account/handlers/admin.get-apartment-expense.handler";
import {AdminGetExpensesSumForPeriodHandler} from "./account/handlers/admin.get-expenses-sum-for-period.handler";
import {AdminDeleteApartmentFacilitiesHandler} from "./account/handlers/admin.delete-apartment-facilities.handler";
import {AdminGetExpensesListHandler} from "./account/handlers/admin.get-expenses-list.handler";
import {AdminUpdateStaffHandler} from "./account/handlers/admin.update-staff.handler";
import {AdminDeleteStaffHandler} from "./account/handlers/admin.delete-staff.handler";
import {DashboardGetReviewListHandler} from "./account/handlers/dashboard.get-review-list.handler";
import {cors} from "./middleware/cors";
import {AdminUpdateIsAdminFlagHandler} from "./account/handlers/admin.update-is-admin-flag.handler";
import {AdminGetUserListHandler} from "./account/handlers/admin.get-user-list.handler";

async function connectToDatabase() {
    GlobalData.connection = await createConnection({
        type: 'postgres',
        host: "localhost",
        port: 5432,
        username: "mini_hotel_api",
        password: "password",
        database: "mini_hotel_api",
        logging: false
    });

    await GlobalData.connection.query(`
        create table IF NOT EXISTS users (
        id serial NOT NULL unique,
    
        login varchar NOT NULL unique,
        password varchar NOT NULL,
    
        "isAdmin" bool NOT NULL default false,
    
        "lastName" varchar,
        name varchar,
        "secondName" varchar,
        "passportDetails" varchar,
        phone varchar unique
    );
    
    create table IF NOT EXISTS apartments (
        id serial NOT NULL unique,
        "roomNumber" varchar,
        square numeric,
        floor INT,
        "costPerDay" numeric,
        address varchar,
        description varchar,
        status varchar NOT NULL
    );
    
    create table IF NOT EXISTS reviews (
        id serial NOT NULL unique,
        "userId" INT REFERENCES users (id),
        "apartmentId" INT REFERENCES apartments (id),
        "createDate" date NOT NULL,
        message varchar,
        score INT
    );
    
    create table IF NOT EXISTS rent (
        id serial NOT NULL unique,
        "rentData" date NOT NULL,
        "apartmentId" INT NOT NULL,
        "userId" INT NOT NULL,
        term INT NOT NULL,
        price numeric,
        PRIMARY KEY ("apartmentId", "rentData")
    );
    
    create table IF NOT EXISTS "expensesTypes" (
        id serial NOT NULL unique,
        name varchar
    );
    
    create table IF NOT EXISTS "apartmentExpenses" (
        id serial NOT NULL unique,
        "apartmentId" INT REFERENCES apartments (id),
        "expenseId" INT REFERENCES "expensesTypes" (id),
        "paymentDate" date NOT NULL,
        price numeric
    );
    
    create table IF NOT EXISTS staff (
        id serial NOT NULL unique,
        "lastName" varchar,
        name varchar,
        "secondName" varchar,
        "position" varchar NOT NULL,
        salary numeric,
        "passportData" varchar NOT NULL,
        phone varchar
    );
    
    create table IF NOT EXISTS payroll (
        id serial NOT NULL unique,
        "staffId" INT REFERENCES staff (id),
        "paymentDate" date NOT NULL,
        "paymentAmount" numeric NOT NULL
    );
    
    create table IF NOT EXISTS staff_apartments (
        id serial NOT NULL unique,
        "staffId" INT REFERENCES staff (id),
        "apartmentId" INT REFERENCES apartments (id)
    );
    
    create table IF NOT EXISTS facilities (
        id serial NOT NULL unique,
        name varchar
    );
    
    create table IF NOT EXISTS "apartmentFacilities" (
        id serial NOT NULL unique,
        "apartmentId" INT REFERENCES apartments (id),
        "facilitiesId" INT REFERENCES facilities (id),
        value varchar
    );`);
}

async function main() {
    const app = express();
    app.use(BodyParser());
    app.use(cors());

    await connectToDatabase();

    app.get('/dashboard/get-apartments/', DashboardGetApartmentsHandler);
    app.get('/dashboard/get-apartment-facilities/', DashboardGetApartmentFacilitiesHandler);
    app.get('/admin/get-expenses-list/', AdminGetExpensesListHandler);
    app.get('/admin/get-staff-list/', AdminGetStaffListHandler);
    app.get('/dashboard/get-review-list/', DashboardGetReviewListHandler);
    app.get('/admin/get-user-list/', AdminGetUserListHandler);

    app.post('/admin/get-expenses-sum-for-period/', AdminGetExpensesSumForPeriodHandler);
    app.post('/admin/get-apartment-expense/', AdminGetApartmentExpenseHandler);

    app.post('/account/signin/', AccountSignInHandler);
    app.post('/account/login/', AccountAuthorizationHandler);
    app.post('/account/add-review/', AccountAddReviewHandler);
    app.post('/admin/add-apartment/', AdminAddApartmentHandler);
    app.post('/admin/add-staff/', AdminAddStaffHandler);
    app.post('/admin/add-rent/', AdminAddRentHandler);
    app.post('/admin/add-apartment-facilities/', AdminAddApartmentFacilitiesHandler);
    app.post('/admin/add-apartment-expenses/', AdminAddApartmentExpenseHandler);
    app.post('/admin/add-payout/', AdminAddPayoutHandler);
    app.post('/admin/add-staff-apartments/', AdminAddStaffApartmentHandler);

    app.put('/admin/update-apartment/', AdminUpdateApartmentHandler);
    app.put('/admin/update-staff/', AdminUpdateStaffHandler);
    app.put('/admin/update-is-admin-flag/', AdminUpdateIsAdminFlagHandler);

    app.delete('/admin/delete-apartment/', AdminDeleteApartmentHandler);
    app.delete('/admin/delete-staff/', AdminDeleteStaffHandler);
    app.delete('/admin/delete-apartment-facilities/', AdminDeleteApartmentFacilitiesHandler);


    app.listen(3333, function () {
        console.log('Example app listening on port 3333!');
    });
}

main();