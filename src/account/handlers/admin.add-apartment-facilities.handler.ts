import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {InAdminAddStaffDTO} from "../dto/in.admin.add-staff.dto";
import {InAdminAddRentDTO} from "../dto/in.admin.add-rent.dto";
import {FacilitiesTypes, InAdminAddApartmentFacilitiesDTO} from "../dto/in.admin.add-apartment-facilities.dto";

async function createApartmentFacilities(dto: InAdminAddApartmentFacilitiesDTO) {
    const facilitiesId = FacilitiesTypes[dto.facilitiesName];

    const queryString = `
      insert into "apartmentFacilities" 
      ("apartmentId", "facilitiesId", "value") 
      values ($1, $2, $3)`;

    const parameters = [ dto.apartmentId, facilitiesId, dto.value ];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AdminAddApartmentFacilitiesHandler(request: Request, response: Response) {
    const dto = new InAdminAddApartmentFacilitiesDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.status(400).send(errors);
        return;
    }

    try {
        await createApartmentFacilities(dto);
    } catch (e) {
        response.status(400).send({
            error: "Some Error while adding new facilities",
        });
        return;
    }

    response.status(201).end();
}