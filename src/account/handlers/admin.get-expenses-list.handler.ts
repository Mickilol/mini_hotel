import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";

export async function AdminGetExpensesListHandler(request: Request, response: Response) {

    const queryString = `
      select "id", "name" from "expensesTypes" order by id`;

    const queryResult: {
        id: number,
        name: string,
    }[] =  await GlobalData.connection.query(queryString);

    response.send(queryResult);
}