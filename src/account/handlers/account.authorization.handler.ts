import {InAccountAuthorizationDto} from "../dto/in.account.authorization.dto";
import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";

export async function AccountAuthorizationHandler(request: Request, response: Response) {
    const dto = new InAccountAuthorizationDto(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.send(errors);
        return;
    }

    const queryString = 'select id, "isAdmin" from users where login = $1 and password = $2;';
    const parameters = [ dto.login, dto.password ];
    const queryResult: { id: number, isAdmin: boolean }[] = await GlobalData.connection.query(queryString, parameters);

    if (queryResult.length === 0) {
        response.status(400).send({
            error: "Incorrect login or password",
        });
        return;
    }

    response.send(queryResult[0]);
}