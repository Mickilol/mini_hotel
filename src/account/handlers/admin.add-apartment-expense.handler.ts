import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {InAdminAddStaffDTO} from "../dto/in.admin.add-staff.dto";
import {InAdminAddRentDTO} from "../dto/in.admin.add-rent.dto";
import {InAdminAddApartmentFacilitiesDTO} from "../dto/in.admin.add-apartment-facilities.dto";
import {InAdminAddApartmentExpenseDTO} from "../dto/in.admin.add-apartment-expense.dto";

async function createApartmentExpense(dto: InAdminAddApartmentExpenseDTO) {
    const queryString = `
      insert into "apartmentExpenses" 
      ("apartmentId", "expenseId", "paymentDate", "price", "period") 
      values ($1, $2, $3, $4, $5)`;

    const parameters = [ dto.apartmentId, dto.expenseId, dto.paymentDate, dto.price, dto.period ];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AdminAddApartmentExpenseHandler(request: Request, response: Response) {
    const dto = new InAdminAddApartmentExpenseDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.status(400).send(errors);
        return;
    }

    try {
        await createApartmentExpense(dto);
    } catch (e) {
        response.status(400).send({
            error: "Some Error while creating new apartment expense",
        });
        return;
    }

    response.status(201).end();
}