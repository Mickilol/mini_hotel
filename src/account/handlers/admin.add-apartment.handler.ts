import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {InAdminAddApartmentDTO} from "../dto/in.admin.add-apartment.dto";

async function createApartment(dto: InAdminAddApartmentDTO) {
    const queryString = `
      insert into apartments 
      ("roomNumber", "square", "floor", "costPerDay", "address", "description", "status", "apartmentNumber", "x", "y", "preview") 
      values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`;

    const parameters = [ dto.roomNumber, dto.square, dto.floor, dto.costPerDay, dto.address, dto.description,
        dto.status , dto.apartmentNumber, dto.x, dto.y, dto.preview];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AdminAddApartmentHandler(request: Request, response: Response) {
    const dto = new InAdminAddApartmentDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.status(400).send(errors);
        return;
    }

    try {
        await createApartment(dto);
    } catch (e) {
        response.status(400).send({
            error: "Some Error while creating new apartment",
        });
        return;
    }

    response.status(201).end();
}