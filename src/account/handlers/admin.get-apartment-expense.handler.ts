import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {validate} from "class-validator";
import { InAdminGetApartmentExpenseDTO} from "../dto/in.admin.get-apartment-expense.dto";

async function getApartmentExpense(dto: InAdminGetApartmentExpenseDTO) {
    const paymentDateUnder = `${+dto.paymentYear - 1}-12-31`;
    const paymentDateUpper = `${+dto.paymentYear + 1}-01-01`;

    const queryString = `
      select address, "apartmentNumber", sum(price)  from "apartmentExpenses" as apart_exp
        JOIN "expensesTypes" as expense ON apart_exp."expenseId" = expense.id
        JOIN apartments ON apart_exp."apartmentId" = apartments.id
        where apart_exp."paymentDate" > $1 and apart_exp."paymentDate" < $2 and apart_exp.period = $3 and apartments.id = $4
        GROUP BY address, "apartmentNumber"`;

    const parameters = [ paymentDateUnder, paymentDateUpper, dto.period, dto.apartmentId ];
    return await GlobalData.connection.query(queryString, parameters);
}

export async function AdminGetApartmentExpenseHandler(request: Request, response: Response) {
    const dto = new InAdminGetApartmentExpenseDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.send(errors);
        return;
    }

    let queryResult: {
        address: string,
        apartmentNumber: string,
        sum: number
    }[];

    try {
        queryResult = await getApartmentExpense(dto);
    } catch (e) {
        response.send({
            error: "Some Error while creating new staff",
        });
        return;
    }

    response.send(queryResult);
}