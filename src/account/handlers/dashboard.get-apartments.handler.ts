import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";

export async function DashboardGetApartmentsHandler(request: Request, response: Response) {

    const queryString = `
      select "id", "roomNumber", "square", "floor", "costPerDay", "address", "description", "status", "apartmentNumber", "x", "y", "preview", "isDeleted"
      from apartments order by id
    `;

    const queryResult: {
        id: number,
        roomNumber: string,
        square: number,
        floor: number,
        costPerDay: number,
        address: string,
        description: string,
        status: string,
        apartmentNumber: string,
        x: number,
        y: number,
        preview: string,
        isDeleted: boolean
    }[] =  await GlobalData.connection.query(queryString);
    
    const responseResult: any[] = [];
    
    for (const item of queryResult) {
        responseResult.push({
            preview: {
              photo: item.preview,
            },
            offer: {
                id: item.id,
                roomNumber: item.roomNumber,
                square: item.square,
                floor: item.floor,
                costPerDay: item.costPerDay,
                address: item.address,
                description: item.description,
                status: item.status,
                apartmentNumber: item.apartmentNumber
            },
            location: {
                x: item.x,
                y: item.y,
            },
            isDeleted: item.isDeleted
        });
    }

    response.send(responseResult);
}