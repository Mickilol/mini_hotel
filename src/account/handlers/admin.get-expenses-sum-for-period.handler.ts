import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {validate} from "class-validator";
import {InAdminGetExpensesSumForPeriodDTO} from "../dto/in.admin.get-expenses-sum-for-period.dto";

async function getExpensesSum(dto: InAdminGetExpensesSumForPeriodDTO) {
    const paymentDateUnder = `${+dto.paymentYear - 1}-12-31`;
    const paymentDateUpper = `${+dto.paymentYear + 1}-01-01`;

    const queryString = `
      select period,  sum(price)  from "apartmentExpenses" as apart_exp
        JOIN "expensesTypes" as expense ON apart_exp."expenseId" = expense.id
        JOIN apartments ON apart_exp."apartmentId" = apartments.id
        where apart_exp."paymentDate" > $1 and apart_exp."paymentDate" < $2 and apart_exp.period = $3
        GROUP BY period`;

    const parameters = [ paymentDateUnder, paymentDateUpper, dto.period ];
    return await GlobalData.connection.query(queryString, parameters);
}

export async function AdminGetExpensesSumForPeriodHandler(request: Request, response: Response) {
    const dto = new InAdminGetExpensesSumForPeriodDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.status(400).send(errors);
        return;
    }

    let queryResult: {
        period: string,
        sum: number
    }[];

    try {
        queryResult = await getExpensesSum(dto);
    } catch (e) {
        response.status(400).send({
            error: "Some Error while creating new staff",
        });
        return;
    }

    response.send(queryResult);
}