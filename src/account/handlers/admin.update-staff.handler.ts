import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {InAdminUpdateStaffDTO} from "../dto/in.admin.update-staff.dto";

async function updateStaff(dto: InAdminUpdateStaffDTO) {
    const queryString = `
      update staff set
           "lastName" = $2, 
           "name" = $3, 
           "secondName" = $4, 
           "position" = $5, 
           "salary" = $6, 
           "passportData" = $7, 
           "phone" = $8
        where id = $1
      `;

    const parameters = [ dto.id, dto.lastName, dto.name, dto.secondName, dto.position, dto.salary, dto.passportData, dto.phone];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AdminUpdateStaffHandler(request: Request, response: Response) {
    const dto = new InAdminUpdateStaffDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.status(400).send(errors);
        return;
    }

    try {
        await updateStaff(dto);
    } catch (e) {
        response.status(400).send({
            error: "Some Error while updating staff",
        });
        return;
    }

    response.status(201).end();
}