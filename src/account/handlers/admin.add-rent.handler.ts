import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {InAdminAddStaffDTO} from "../dto/in.admin.add-staff.dto";
import {InAdminAddRentDTO} from "../dto/in.admin.add-rent.dto";

async function createRent(dto: InAdminAddRentDTO) {
    const queryString = `
      insert into rent 
      ("rentDate", "apartmentId", "userId", "term", "price") 
      values ($1, $2, $3, $4, $5)`;

    const parameters = [ dto.rentDate, dto.apartmentId, dto.userId, dto.term, dto.price ];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AdminAddRentHandler(request: Request, response: Response) {
    const dto = new InAdminAddRentDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.status(400).send(errors);
        return;
    }

    try {
        await createRent(dto);
    } catch (e) {
        response.status(400).send({
            error: "Some Error while creating new rent",
        });
        return;
    }

    response.status(201).end();
}