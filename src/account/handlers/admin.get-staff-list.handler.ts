import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";

export async function AdminGetStaffListHandler(request: Request, response: Response) {

    const queryString = `
      select "id", "lastName", "name", "secondName", "position", "salary", "passportData", "phone"
      from staff order by id
    `;

    const queryResult: {
        id: number,
        lastName: string,
        name: string,
        secondName: string,
        position: string,
        salary: number,
        passportData: string,
        phone: string
    }[] =  await GlobalData.connection.query(queryString);

    response.send(queryResult);
}