import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {InAccountSignInDTO} from "../dto/in.account.sign-in.dto";

async function createUser(dto: InAccountSignInDTO) {
    const queryString = `
      insert into users 
      ("login", "password", "lastName", "name", "secondName", "passportDetails", "phone") 
      values ($1, $2, $3, $4, $5, $6, $7)`;

    const parameters = [ dto.login, dto.password, dto.lastName, dto.name, dto.secondName, dto.passportDetails, dto.phone ];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AccountSignInHandler(request: Request, response: Response) {
    const dto = new InAccountSignInDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.status(400).send(errors);
        return;
    }

    try {
        await createUser(dto);
    } catch (e) {
        response.status(400).send({
            error: "Login or phone already taken",
        });
        return;
    }

    response.status(201).end();
}