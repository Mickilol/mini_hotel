import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {InAdminUpdateApartmentDTO} from "../dto/in.admin.update-apartment.dto";

async function updateApartment(dto: InAdminUpdateApartmentDTO) {
    const queryString = `
      update apartments set
           "roomNumber" = $2, 
           "square" = $3, 
           "floor" = $4, 
           "costPerDay" = $5, 
           "address" = $6, 
           "description" = $7, 
           "status" = $8,
           "apartmentNumber" = $9,
            "x" =  $10,
            "y" = $11,
            "preview" = $12
        where id = $1
      `;

    const parameters = [ dto.id, dto.roomNumber, dto.square, dto.floor, dto.costPerDay, dto.address, dto.description,
        dto.status, dto.apartmentNumber, dto.x, dto.y, dto.preview ];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AdminUpdateApartmentHandler(request: Request, response: Response) {
    const dto = new InAdminUpdateApartmentDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.status(400).send(errors);
        return;
    }

    try {
        await updateApartment(dto);
    } catch (e) {
        response.status(400).send({
            error: "Some Error while updating apartment",
        });
        return;
    }

    response.status(201).end();
}