import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";

export async function DashboardGetReviewListHandler(request: Request, response: Response) {
    const queryString = `
      select "name", "address", "createDate", "message", "score" from reviews 
             join users on reviews."userId" = users.id
             join apartments on reviews."apartmentId" = apartments.id`
    ;

    const queryResult: {
        name: number;
        address: number;
        createDate: string;
        message: string;
        score: number;
    }[] =  await GlobalData.connection.query(queryString);

    response.send(queryResult);
}