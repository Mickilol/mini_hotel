import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {InAdminAddStaffApartmentsDTO} from "../dto/in.admin.add-staff-apartments.dto";
import {InAdminUpdateIsAdminFlagDTO} from "../dto/in.admin.update-is-admin-flag.dto";

async function updateIsAdminFlag(dto: InAdminUpdateIsAdminFlagDTO) {
    const queryString = `
      update users set 
          "isAdmin" = $2
      where id = $1`;

    const parameters = [ dto.userId, dto.isAdminFlag ];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AdminUpdateIsAdminFlagHandler(request: Request, response: Response) {
    const dto = new InAdminUpdateIsAdminFlagDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.send(errors);
        return;
    }

    try {
        await updateIsAdminFlag(dto);
    } catch (e) {
        response.send({
            error: "Some Error while updating isAdmin flag",
        });
        return;
    }

    response.status(201).end();
}