import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";

export async function AdminGetUserListHandler(request: Request, response: Response) {

    const queryString = `
      select "id", "login", "isAdmin" from "users" order by id`;

    const queryResult: {
        id: number,
        login: string,
        isAdmin: boolean,
    }[] =  await GlobalData.connection.query(queryString);

    response.send(queryResult);
}