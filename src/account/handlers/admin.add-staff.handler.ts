import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {InAdminAddStaffDTO} from "../dto/in.admin.add-staff.dto";

async function createStaff(dto: InAdminAddStaffDTO) {
    const queryString = `
      insert into staff 
      ("lastName", "name", "secondName", "position", "salary", "passportData", "phone") 
      values ($1, $2, $3, $4, $5, $6, $7)`;

    const parameters = [ dto.lastName, dto.name, dto.secondName, dto.position, dto.salary, dto.passportData, dto.phone ];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AdminAddStaffHandler(request: Request, response: Response) {
    const dto = new InAdminAddStaffDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.status(400).send(errors);
        return;
    }

    try {
        await createStaff(dto);
    } catch (e) {
        response.status(400).send({
            error: "Some Error while creating new staff",
        });
        return;
    }

    response.status(201).end();
}