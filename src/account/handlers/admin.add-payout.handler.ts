import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {InAdminAddPayoutDTO} from "../dto/in.admin.add-payout.dto";

async function createPayout(dto: InAdminAddPayoutDTO) {
    const queryString = `
      insert into payroll 
      ("staffId", "paymentDate", "paymentAmount") 
      values ($1, $2, $3)`;

    const parameters = [ dto.staffId, dto.paymentDate, dto.paymentAmount ];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AdminAddPayoutHandler(request: Request, response: Response) {
    const dto = new InAdminAddPayoutDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.send(errors);
        return;
    }

    try {
        await createPayout(dto);
    } catch (e) {
        response.send({
            error: "Some Error while creating new payout",
        });
        return;
    }

    response.status(201).end();
}