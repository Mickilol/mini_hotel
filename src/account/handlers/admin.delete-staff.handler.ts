import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {InAdminDeleteApartmentDTO} from "../dto/in.admin.delete-apartment.dto";

async function deleteStaff(dto: InAdminDeleteApartmentDTO) {
    const queryString = `delete from staff where id = $1`;

    const parameters = [ dto.id ];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AdminDeleteStaffHandler(request: Request, response: Response) {
    const dto = new InAdminDeleteApartmentDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.status(400).send(errors);
        return;
    }

    try {
        await deleteStaff(dto);
    } catch (e) {
        response.status(400).send({
            error: "Some Error while removing staff",
        });
        return;
    }

    response.status(200).end();
}