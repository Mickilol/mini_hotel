import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";

export async function DashboardGetApartmentFacilitiesHandler(request: Request, response: Response) {

    const queryString = `
      select  "apartmentId", "name", "value"
      from "apartmentFacilities", facilities
      where "apartmentFacilities"."facilitiesId" = facilities.id
      order by "apartmentId"
    `;

    const queryResult: {
        apartmentId: number,
        name: string,
        value: string
    }[] =  await GlobalData.connection.query(queryString);

    response.send(queryResult);
}