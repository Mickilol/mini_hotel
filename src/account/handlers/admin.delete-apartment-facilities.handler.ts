import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {FacilitiesTypes} from "../dto/in.admin.add-apartment-facilities.dto";
import {InAdminDeleteApartmentFacilitiesDTO} from "../dto/in.admin.delete-apartment-facilities.dto";

async function createApartmentFacilities(dto: InAdminDeleteApartmentFacilitiesDTO) {
    const facilitiesId = FacilitiesTypes[dto.facilitiesName];

    const queryString = `
      delete from "apartmentFacilities" where "apartmentId" = $1 and "facilitiesId" = $2;`;

    const parameters = [ dto.apartmentId, facilitiesId];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AdminDeleteApartmentFacilitiesHandler(request: Request, response: Response) {
    const dto = new InAdminDeleteApartmentFacilitiesDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.status(400).send(errors);
        return;
    }

    try {
        await createApartmentFacilities(dto);
    } catch (e) {
        response.status(400).send({
            error: "Some Error while creating new rent",
        });
        return;
    }

    response.status(201).end();
}