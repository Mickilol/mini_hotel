import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {InAccountAddReviewDTO} from "../dto/in.account.add-review.dto";

async function createReview(dto: InAccountAddReviewDTO) {
    const queryString = `
      insert into reviews 
      ("userId", "apartmentId", "createDate", "message", "score") 
      values ($1, $2, $3, $4, $5)`;

    const parameters = [ dto.userId, dto.apartmentId, dto.createDate, dto.message, dto.score ];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AccountAddReviewHandler(request: Request, response: Response) {
    const dto = new InAccountAddReviewDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.send(errors);
        return;
    }

    try {
        await createReview(dto);
    } catch (e) {
        response.send({
            error: "Some Error while creating new review",
        });
        return;
    }

    response.status(201).end();
}