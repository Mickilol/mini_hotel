import {validate} from "class-validator";
import { Request, Response } from 'express';
import {GlobalData} from "../../global-data";
import {InAdminAddStaffApartmentsDTO} from "../dto/in.admin.add-staff-apartments.dto";

async function createStaffApartment(dto: InAdminAddStaffApartmentsDTO) {
    const queryString = `
      insert into "staff_apartments" 
      ("staffId", "apartmentId") 
      values ($1, $2)`;

    const parameters = [ dto.staffId, dto.apartmentId ];
    await GlobalData.connection.query(queryString, parameters);
}

export async function AdminAddStaffApartmentHandler(request: Request, response: Response) {
    const dto = new InAdminAddStaffApartmentsDTO(request.body);

    const errors = await validate(dto);
    if (errors.length > 0) {
        response.send(errors);
        return;
    }

    try {
        await createStaffApartment(dto);
    } catch (e) {
        response.send({
            error: "Some Error while creating new staff-apartment",
        });
        return;
    }

    response.status(201).end();
}