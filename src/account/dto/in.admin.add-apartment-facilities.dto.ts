import {IsNumber, IsString} from "class-validator";

export class InAdminAddApartmentFacilitiesDTO {
    @IsNumber()
    apartmentId: number;

    @IsString()
    facilitiesName: string;

    @IsString()
    value: string;

    constructor(data: Partial<InAdminAddApartmentFacilitiesDTO>) {
        Object.assign(this, data);
    }
}

export enum FacilitiesTypes {
    'удаленность от метро' = 1,
    'лифт' = 2,
    'wifi' =  4,
    'кондиционер' = 5,
    'парковка' = 6,
    'кухня' = 7,
    'стиралка' = 8,
}