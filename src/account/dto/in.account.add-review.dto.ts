import {IsNumber, IsString} from "class-validator";

export class InAccountAddReviewDTO {
    @IsNumber()
    userId: number;

    @IsNumber()
    apartmentId: number;

    @IsString()
    createDate: string;

    @IsString()
    message: string;

    @IsNumber()
    score: number;

    constructor(data: Partial<InAccountAddReviewDTO>) {
        Object.assign(this, data);
    }
}