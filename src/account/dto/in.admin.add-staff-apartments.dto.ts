import {IsNumber} from "class-validator";

export class InAdminAddStaffApartmentsDTO {
    @IsNumber()
    staffId: number;

    @IsNumber()
    apartmentId: number;

    constructor(data: Partial<InAdminAddStaffApartmentsDTO>) {
        Object.assign(this, data);
    }
}