import {IsNumber, IsOptional, IsString, MinLength} from "class-validator";

export class InAdminAddApartmentDTO {
    @IsString()
    roomNumber: string;

    @IsNumber()
    square: number;

    @IsNumber()
    @IsOptional()
    floor?: number;

    @IsNumber()
    costPerDay: number;

    @IsString()
    address: string;

    @IsString()
    @IsOptional()
    description?: string;

    @IsString()
    @IsOptional()
    status?: string;

    @IsString()
    @IsOptional()
    apartmentNumber?: string;

    @IsNumber()
    x: number;

    @IsNumber()
    y: number;

    @IsString()
    @IsOptional()
    preview: string;

    constructor(data: Partial<InAdminAddApartmentDTO>) {
        Object.assign(this, data);
    }
}