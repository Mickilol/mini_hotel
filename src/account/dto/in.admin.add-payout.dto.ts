import {IsNumber} from "class-validator";

export class InAdminAddPayoutDTO {
    @IsNumber()
    staffId: number;

    @IsNumber()
    paymentDate: number;

    @IsNumber()
    paymentAmount: number;

    constructor(data: Partial<InAdminAddPayoutDTO>) {
        Object.assign(this, data);
    }
}