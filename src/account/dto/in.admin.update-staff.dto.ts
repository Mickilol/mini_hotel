import {IsNumber} from "class-validator";
import {InAdminAddStaffDTO} from "./in.admin.add-staff.dto";

export class InAdminUpdateStaffDTO extends InAdminAddStaffDTO {
    @IsNumber()
    id: number;

    constructor(data: Partial<InAdminUpdateStaffDTO>) {
        super(data);
        Object.assign(this, data);
    }
}