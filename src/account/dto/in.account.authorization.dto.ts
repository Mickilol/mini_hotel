import {IsString} from "class-validator";

export class InAccountAuthorizationDto {
    @IsString()
    login: string;

    @IsString()
    password: string;

    constructor(data: Partial<InAccountAuthorizationDto>) {
        Object.assign(this, data);
    }
}