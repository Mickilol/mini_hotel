import {IsNumber, IsOptional, IsString, } from "class-validator";
import {InAdminAddApartmentDTO} from "./in.admin.add-apartment.dto";

export class InAdminUpdateApartmentDTO extends InAdminAddApartmentDTO {
    @IsNumber()
    id: number;

    constructor(data: Partial<InAdminUpdateApartmentDTO>) {
        super(data);
        Object.assign(this, data);
    }
}