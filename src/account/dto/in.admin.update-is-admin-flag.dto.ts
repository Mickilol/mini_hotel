import {IsBoolean, IsNumber} from "class-validator";

export class InAdminUpdateIsAdminFlagDTO {
    @IsNumber()
    userId: number;

    @IsBoolean()
    isAdminFlag: boolean;

    constructor(data: Partial<InAdminUpdateIsAdminFlagDTO>) {
        Object.assign(this, data);
    }
}