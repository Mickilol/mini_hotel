import {IsNumber, IsOptional, IsString} from "class-validator";

export class InAdminAddStaffDTO {
    @IsString()
    @IsOptional()
    lastName?: string;

    @IsString()
    @IsOptional()
    name?: string;

    @IsString()
    @IsOptional()
    secondName?: string;

    @IsString()
    position: string;

    @IsNumber()
    salary: number;

    @IsString()
    passportData: string;

    @IsString()
    @IsOptional()
    phone?: string;

    constructor(data: Partial<InAdminAddStaffDTO>) {
        Object.assign(this, data);
    }
}