import {IsNumber, IsString} from "class-validator";

export class InAdminDeleteApartmentFacilitiesDTO {
    @IsNumber()
    apartmentId: number;

    @IsString()
    facilitiesName: string;

    constructor(data: Partial<InAdminDeleteApartmentFacilitiesDTO>) {
        Object.assign(this, data);
    }
}