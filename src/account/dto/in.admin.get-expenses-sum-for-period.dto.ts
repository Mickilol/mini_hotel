
import {IsString} from "class-validator";

export class InAdminGetExpensesSumForPeriodDTO {
    @IsString()
    period: string;

    @IsString()
    paymentYear: string;

    constructor(data: Partial<InAdminGetExpensesSumForPeriodDTO>) {
        Object.assign(this, data);
    }
}