import {IsNumber, IsString} from "class-validator";

export class InAdminAddApartmentExpenseDTO {
    @IsNumber()
    apartmentId: number;

    @IsNumber()
    expenseId: number;

    @IsString()
    paymentDate: string;

    @IsNumber()
    price: number;

    @IsString()
    period: string;

    constructor(data: Partial<InAdminAddApartmentExpenseDTO>) {
        Object.assign(this, data);
    }
}