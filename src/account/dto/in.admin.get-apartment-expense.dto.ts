
import {IsNumber, IsString} from "class-validator";

export class InAdminGetApartmentExpenseDTO {
    @IsNumber()
    apartmentId: number;

    @IsString()
    period: string;

    @IsString()
    paymentYear: string;

    constructor(data: Partial<InAdminGetApartmentExpenseDTO>) {
        Object.assign(this, data);
    }
}