import {IsOptional, IsString, MinLength} from "class-validator";

export class InAccountSignInDTO {
    @IsString()
    @MinLength(5)
    login: string;

    @IsString()
    @MinLength(5)
    password: string;

    @IsString()
    @IsOptional()
    lastName?: string;

    @IsString()
    @IsOptional()
    name?: string;

    @IsString()
    @IsOptional()
    secondName?: string;

    @IsString()
    @IsOptional()
    passportDetails?: string;

    @IsString()
    @IsOptional()
    phone?: string;

    constructor(data: Partial<InAccountSignInDTO>) {
        Object.assign(this, data);
    }
}