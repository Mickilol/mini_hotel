import {IsNumber, IsString} from "class-validator";

export class InAdminAddRentDTO {
    @IsString()
    rentDate: string;

    @IsNumber()
    apartmentId: number;

    @IsNumber()
    userId: number;

    @IsNumber()
    term: number;

    @IsNumber()
    price: number;

    constructor(data: Partial<InAdminAddRentDTO>) {
        Object.assign(this, data);
    }
}