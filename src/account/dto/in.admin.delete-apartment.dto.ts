import {IsNumber } from "class-validator";

export class InAdminDeleteApartmentDTO {
    @IsNumber()
    id: number;

    constructor(data: Partial<InAdminDeleteApartmentDTO>) {
        Object.assign(this, data);
    }
}